package com.example.self_pickuppoint;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import com.example.self_pickuppoint.DatabaseModule.GoodsRecord;
import com.example.self_pickuppoint.DatabaseModule.DatabaseHelper;

public class GoodsRecordActivity extends AppCompatActivity {
    private TextView tv_record;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goods_record);
        String mid = getIntent().getStringExtra("mid");
        tv_record = findViewById(R.id.tv_record);
        searchRecord(mid);

    }

    public void searchRecord(String mid) {
        List<GoodsRecord> record = new ArrayList<>();
        String display = "Purchase record:\n";
        DatabaseHelper db = new DatabaseHelper(this);
        record = db.getAllRecord(mid);
        for (GoodsRecord goodsrecord : record) {
            display += "Goods Name:" + goodsrecord.getGoodsname()
                    + "\n\nBought Date:" + goodsrecord.getBoughtdate()
                    + "\n\nDescription: \n" + goodsrecord.getDesc()
                    + "\n\nPlace:" + goodsrecord.getGoodsplace();
        }
        tv_record.setText(display);
    }
}
