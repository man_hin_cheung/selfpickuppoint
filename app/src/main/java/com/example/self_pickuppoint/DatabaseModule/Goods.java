package com.example.self_pickuppoint.DatabaseModule;

public class Goods {
    private String bid, goodsname, goodsdesc,goodsplace;
    private int number;

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public String getGoodsname() {
        return goodsname;
    }

    public void setGoodsname(String goodsname) {
        this.goodsname = goodsname;
    }

    public String getGoodsdesc() {
        return goodsdesc;
    }

    public void setGoodsdesc(String goodsdesd) {
        this.goodsdesc = goodsdesd;
    }

    public String getGoodsplace() {
        return goodsplace;
    }

    public void setGoodsplace(String goodsplace) {
        this.goodsplace = goodsplace;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Goods() {
    }

    public Goods(String bid, String goodsname, String goodsdesc,String goodsplace, int number) {
        this.bid = bid;
        this.goodsname = goodsname;
        this.goodsdesc = goodsdesc;
        this.goodsplace = goodsplace;
        this.number = number;
    }
}
