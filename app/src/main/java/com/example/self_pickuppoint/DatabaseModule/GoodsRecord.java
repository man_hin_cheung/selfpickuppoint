package com.example.self_pickuppoint.DatabaseModule;

public class GoodsRecord {
    private String goodsname, boughtdate, goodsdesc,goodsplace;

    public GoodsRecord() {
    }

    public String getGoodsname() {
        return goodsname;
    }

    public void setGoodsname(String goodsname) {
        this.goodsname = goodsname;
    }

    public String getBoughtdate() {
        return boughtdate;
    }

    public void setBoughtdate(String boughtdate) {
        this.boughtdate = boughtdate;
    }

    public String getDesc() {
        return goodsdesc;
    }

    public void setDesc(String goodsdesc) {
        this.goodsdesc = goodsdesc;
    }

    public String getGoodsplace() {
        return goodsplace;
    }

    public void setGoodsplace(String goodsplace) {
        this.goodsplace = goodsplace;
    }

}
