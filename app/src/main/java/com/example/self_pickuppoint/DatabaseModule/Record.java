package com.example.self_pickuppoint.DatabaseModule;

public class Record {
    private int rid, mid, bid;
    private String boughtdate;

    public Record(int mid, int bid, String boughtdate) {
        this.mid = mid;
        this.bid = bid;
        this.boughtdate = boughtdate;
    }

    public Record() {

    }

    public int getRid() {
        return rid;
    }

    public void setRid(int rid) {
        this.rid = rid;
    }

    public int getMid() {
        return mid;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    public int getBid() {
        return bid;
    }

    public void setBid(int bid) {
        this.bid = bid;
    }

    public String getBoughtdate() {
        return boughtdate;
    }

    public void setBoughtdate(String boughtdate) {
        this.boughtdate = boughtdate;
    }
}
