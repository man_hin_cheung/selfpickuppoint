package com.example.self_pickuppoint.DatabaseModule;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
    //Database Version
    private static final int DATABASE_VERSION = 1;
    //Database Name
    private static final String DATABASE_NAME = "selfpickuppoint";
    //Member table name
    private static final String TABLE_MEMBER = "member";
    //Member Table Columns name
    private static final String COLUMN_MID = "mid";
    private static final String COLUMN_USERNAME = "username";
    private static final String COLUMN_PASSWORD = "password";
    private static final String COLUMN_AGE = "age";
    //Goods table name
    private static final String TABLE_GOODS = "goods";
    //Goods Table Columns name
    private static final String COLUMN_BID = "bid";
    private static final String COLUMN_GOODSNAME = "goodsname";
    private static final String COLUMN_GOODSDESC = "goodsdesc";
    private static final String COLUMN_GOODSPLACE = "goodsplace";
    private static final String COLUMN_NUMBER = "number";

    //Record table name
    private static final String TABLE_RECORD = "record";
    //Record Table Columns name
    private static final String COLUMN_RID = "rid";
    private static final String COLUMN_BOUGHTDATE = "boughtdate";

    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db){
        String sql;

        // Create table
        sql = "CREATE TABLE Member(" + "Mid int PRIMARY KEY ,"
                + "Username text NOT NULL, " + "Password text NOT NULL, " + "Age int NOT NULL); ";
        db.execSQL(sql);
        sql = "CREATE TABLE Goods(" + "Bid int PRIMARY KEY ,"
                + "Goodsname text, " + "Goodsdesc text, " + "Goodsplace text, " + "number int); ";
        db.execSQL(sql);
        sql = "CREATE TABLE Record(" + "Rid int PRIMARY KEY ,"
                + "mid int, " + "bid int, " + "boughtdate datetime NOT NULL DEFAULT CURRENT_TIMESTAMP, " +
                " CONSTRAINT fk_goods_record FOREIGN KEY (bid) REFERENCES Goods (bid), " +
                " CONSTRAINT fk_member_record FOREIGN KEY (mid) REFERENCES Member (mid)); ";
        db.execSQL(sql);

        // Add member rows
        db.execSQL("INSERT INTO Member(Mid, Username, Password, Age) values"
                + "(1001, 'test', '12345', 19); ");
        db.execSQL("INSERT INTO Member(Mid, Username, Password, Age) values"
                + "(1002, 'adriancheung', 'password', 25); ");
        db.execSQL("INSERT INTO Member(Mid, Username, Password, Age) values"
                + "(1003, '207108059', '123456', 20); ");


        // Add goods rows
        db.execSQL("INSERT INTO Goods(bid, goodsname, goodsdesc,goodsplace, number) values"
                + "(1, 'GD1001', " +
                "'Mirrors x12 \n" +
                "Bathmats x1 \n" +
                "Towels x3','Shatin SF', 1); ");

        db.execSQL("INSERT INTO Goods(bid, goodsname, goodsdesc,goodsplace, number) values"
                + "(2, 'GD1002', " +
                "'Curtains x2 \n" +
                "Dining table x1 \n" +
                "office desk x1','Yuen Long SF', 1); ");

        db.execSQL("INSERT INTO Goods(bid, goodsname, goodsdesc,goodsplace, number) values"
                + "(3, 'GD1003', " +
                "'Sofa bed x1 \n" +
                "Bookcase x3 \n" +
                "Phone case x1 \n" +
                "Rugs x3','Kwun Tong SF', 1); ");

        db.execSQL("INSERT INTO Goods(bid, goodsname, goodsdesc,goodsplace, number) values"
                + "(4, 'GD1004', " +
                "'Sofas x2 \n" +
                "Quilts x3 \n" +
                "Pans x2','Mong Kok SF', 1); ");

        db.execSQL("INSERT INTO Goods(bid, goodsname, goodsdesc,goodsplace, number) values"
                + "(5, 'GD1005', " +
                "'Foods x10 \n" +
                "Cutlery x4 \n" +
                "Lamps x3 \n \n \n','Wan Chai SF', 1); ");

        // Add member rows
        db.execSQL("INSERT INTO Record(rid, mid, bid, boughtdate) values"
                + "(1, 1001, 1, '2021-05-09 17:31:54'); ");

        db.execSQL("INSERT INTO Record(rid, mid, bid, boughtdate) values"
                + "(2, 1002, 3, '2021-05-05 13:12:18'); ");

    }

    //Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int odlVersion, int newVersion){
        //Drop older table if existed
        db.execSQL("DROP TABLE if exists Member;");
        db.execSQL("DROP TABLE if exists Goods;");
        db.execSQL("DROP TABLE if exists Record;");
        //Create tables again
        onCreate(db);
    }

    public String checkMember(String usernameInput, String passwordInput){
        String mid = "no";
        String loginQuery  = "SELECT " + COLUMN_MID + " FROM " + TABLE_MEMBER + " WHERE " + COLUMN_USERNAME + " = '" +
                usernameInput + "' AND " + COLUMN_PASSWORD + " = '" + passwordInput + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(loginQuery, null);
        //looping
        if(cursor.moveToNext()) {
            mid = cursor.getString(0);
        }
        db.close();
        return mid;
    }

    public List<Goods> searchGoods(String keyword){
        List<Goods> goodsList = new ArrayList<Goods>();
        //Select All Query about the keyword
        String searchQuery = "SELECT * FROM " + TABLE_GOODS + " WHERE " + COLUMN_GOODSNAME + " LIKE '%" + keyword + "%' " ;
                //"OR " + COLUMN_GOODSDESC + " LIKE '%" + keyword + "%'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(searchQuery, null);
        //looping
        if(cursor.moveToFirst()) {
            do{
                Goods goods = new Goods();
                goods.setBid(cursor.getString(0));
                goods.setGoodsname(cursor.getString(1));
                goods.setGoodsdesc(cursor.getString(2));
                goods.setGoodsplace(cursor.getString(3));
                goods.setNumber(Integer.parseInt(cursor.getString(4)));
                //Adding goods to list
                goodsList.add(goods);
            } while (cursor.moveToNext());
        }
        db.close();
        return goodsList;

    }

    public List<Goods> getAllGoods(){
        List<Goods> goodsList = new ArrayList<Goods>();
        //Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_GOODS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        //looping
        if(cursor.moveToFirst()) {
            do{
                Goods goods = new Goods();
                goods.setBid(cursor.getString(0));
                goods.setGoodsname(cursor.getString(1));
                goods.setGoodsdesc(cursor.getString(2));
                goods.setGoodsplace(cursor.getString(3));
                goods.setNumber(Integer.parseInt(cursor.getString(4)));
                //Adding goods to list
                goodsList.add(goods);
            } while (cursor.moveToNext());
        }
        db.close();
        return goodsList;
    }

    public List<GoodsRecord> getAllRecord(String mid){
        List<GoodsRecord> goodsRecordList = new ArrayList<GoodsRecord>();
        //Select All Query
        String selectRecordQuery = "SELECT " + "goods.goodsname, boughtdate, goods.goodsdesc,goods.goodsplace FROM " + TABLE_RECORD + ", " + TABLE_GOODS + ", "
                + TABLE_MEMBER + " WHERE " + TABLE_RECORD + "." + COLUMN_MID + " = " + mid +
                " AND member.mid = record.mid " + " AND goods.bid = record.bid";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectRecordQuery, null);

        //looping
        if(cursor.moveToFirst()) {
            do{
                GoodsRecord goodsRecord = new GoodsRecord();
                goodsRecord.setGoodsname(cursor.getString(0));
                goodsRecord.setBoughtdate(cursor.getString(1));
                goodsRecord.setDesc(cursor.getString(2));
                goodsRecord.setGoodsplace(cursor.getString(3));

                //Adding goods to list
                goodsRecordList.add(goodsRecord);
            } while (cursor.moveToNext());
        }
        db.close();
        return goodsRecordList;
    }

    public String checkMembor(String usernameInput, String passwordInput){
        String mid = "1001";
        return mid;
    }

}
