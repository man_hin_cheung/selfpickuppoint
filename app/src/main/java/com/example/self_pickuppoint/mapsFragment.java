package com.example.self_pickuppoint;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import androidx.core.content.ContextCompat;
import com.example.self_pickuppoint.DurationModule.DirectionFinderListener;
import com.example.self_pickuppoint.DurationModule.DirectionFinder;
import com.example.self_pickuppoint.DurationModule.Route;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import static android.content.Context.LOCATION_SERVICE;


public class mapsFragment extends Fragment implements OnMapReadyCallback, LocationListener, DirectionFinderListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private static boolean rLocationGranted = false;
    private static int LOCATION_PERMISSION_REQUEST_CODE = 1001;
    private GoogleMap mMap;
    private LocationManager locMgr;
    private String bestProv;
    private Button btnFindSTPath;
    private Button btnFindYLPath;
    private Button btnFindTKOPath;
    private Button btnFindMKPath;
    private Button btnFindKTPath;
    private Button btnFindCSWPath;
    private Button btnFindSWPath;
    private Button btnFindWCPath;
    private Button btnFindSKWPath;

    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();
    TextView textView;
    TextView textView2;

    public mapsFragment() {
        // Required empty public constructor
    }

    public static mapsFragment newInstance(String param1, String param2) {
        mapsFragment fragment = new mapsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_maps, container, false);

        btnFindSTPath = v.findViewById(R.id.btnFindSTPath);
        btnFindYLPath = v.findViewById(R.id.btnFindYLPath);
        btnFindTKOPath = v.findViewById(R.id.btnFindTKOPath);

        btnFindMKPath = v.findViewById(R.id.btnFindMKPath);
        btnFindKTPath = v.findViewById(R.id.btnFindKTPath);
        btnFindCSWPath = v.findViewById(R.id.btnFindCSWPath);

        btnFindSWPath = v.findViewById(R.id.btnFindSWPath);
        btnFindWCPath = v.findViewById(R.id.btnFindWCPath);
        btnFindSKWPath = v.findViewById(R.id.btnFindSKWPath);

        textView = v.findViewById(R.id.tvDuration);
        textView2 = v.findViewById(R.id.tvDistance);

        if (chkPlayService() == true) {
            initialMap();
        }
        if (rLocationGranted == true) {
            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);

        }

        // NT
        btnFindSTPath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendRequest(getCurrentPath(),"22.38716378693785, 114.20236756184288");
            }
        });

        btnFindYLPath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendRequest(getCurrentPath(),"22.44385210664754, 114.03172748168713");
            }
        });

        btnFindTKOPath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendRequest(getCurrentPath(),"22.3354665,114.1501536");
            }
        });

        //kl
        btnFindMKPath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendRequest(getCurrentPath(),"22.31735650983803, 114.17156003216627");
            }
        });

        btnFindKTPath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendRequest(getCurrentPath(),"22.31459057694007, 114.22287660685916");
            }
        });

        btnFindCSWPath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendRequest(getCurrentPath(),"22.334873412237283, 114.1610303400543");
            }
        });

        //hk
        btnFindSWPath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendRequest(getCurrentPath(),"22.28865108880125, 114.15200883242463");
            }
        });

        btnFindWCPath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendRequest(getCurrentPath(),"22.281799780362615, 114.1798055196969");
            }
        });

        btnFindSKWPath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendRequest(getCurrentPath(),"22.279778018638766, 114.22653794153015");
            }
        });


        return v;

    }

    @Override
    public void onLocationChanged(Location location) {
        String x = Double.toString(location.getLatitude());
        String y = Double.toString(location.getLongitude());
        LatLng Point = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.clear();
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Point, 15));
        mMap.addMarker(new MarkerOptions().position(Point).title("Your are here!"));
        // Add a marker in library and move the camera
        //LatLng library = new LatLng(22.3903303, 114.1958548);
        //mMap.addMarker(new MarkerOptions().position(library).title("Sha Tin IVE library"));
        sendRequest(x,y);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    @Override
    public void onResume() {
        super.onResume();
        locMgr = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        bestProv = locMgr.getBestProvider(criteria, true);

        if (locMgr.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locMgr.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                locMgr.requestLocationUpdates(bestProv, 100, 1, this);

            }
        } else {
            Toast.makeText(getContext().getApplicationContext(), "Please open ths GPS!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            locMgr.removeUpdates(this);
        }
    }

    private String getCurrentPath() {
        LocationManager locationManager;
        String provider = LocationManager.GPS_PROVIDER;
        //Get location services
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return "TODO";
        }
        Location location = locationManager.getLastKnownLocation(provider);
        String currentPath = location.getLatitude() + "," + location.getLongitude();
        return currentPath;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        rLocationGranted = false;
        switch (requestCode) {
            case 1001: {
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            rLocationGranted = false;
                            return;
                        }
                    }
                    rLocationGranted = true;
                }
            }
        }
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private boolean chkPlayService() {
        int avai = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getContext());
        if (avai == ConnectionResult.SUCCESS) {
            Log.i("Map Test", "version is Fine...");
            return true;
        } else {
            Toast.makeText(getContext().getApplicationContext(), "The version is not allow to run MAP", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    private void initialMap() {
        String[] permissions = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
        if ((ContextCompat.checkSelfPermission(getContext(), permissions[0]) == PackageManager.PERMISSION_GRANTED)
                || ContextCompat.checkSelfPermission(getContext(), permissions[1]) == PackageManager.PERMISSION_GRANTED) {
            rLocationGranted = true;
        } else {
            ActivityCompat.requestPermissions(getActivity(), permissions,
                    LOCATION_PERMISSION_REQUEST_CODE);
        }
    }



    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Criteria criteria = new Criteria();
        bestProv = locMgr.getBestProvider(criteria, true);
    }

    public void onDirectionFinderStart() {
        if (originMarkers != null) {
            for (Marker marker : originMarkers) {
                marker.remove();
            }
        }

        if (destinationMarkers != null) {
            for (Marker marker : destinationMarkers) {
                marker.remove();
            }
        }

        if (polylinePaths != null) {
            for (Polyline polyline : polylinePaths) {
                polyline.remove();
            }
        }
    }

    public void onDirectionFinderSuccess(List<Route> routes) {
        polylinePaths = new ArrayList<>();
        originMarkers = new ArrayList<>();
        destinationMarkers = new ArrayList<>();

        for (Route route : routes) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 12));

            textView.setText(route.duration.getText());
            textView2.setText(route.distance.getText());

            originMarkers.add(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.start_blue))
                    .title(route.startAddress)
                    .position(route.startLocation)));
            destinationMarkers.add(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.end_green))
                    .title(route.endAddress)
                    .position(route.endLocation)));

            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true).
                    color(Color.RED).
                    width(10);

            for (int i = 0; i < route.points.size(); i++)
                polylineOptions.add(route.points.get(i));

            polylinePaths.add(mMap.addPolyline(polylineOptions));
        }
    }
    private void sendRequest(String origin, String destination) {
        try {
            new DirectionFinder(this, origin, destination).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}