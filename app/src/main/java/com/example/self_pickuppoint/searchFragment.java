package com.example.self_pickuppoint;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import com.example.self_pickuppoint.DatabaseModule.Goods;
import com.example.self_pickuppoint.DatabaseModule.DatabaseHelper;

import androidx.fragment.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class searchFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private TextView tv_data;
    private Button btn_searchAll, btn_searchKey;
    private EditText edt_keyword;

    public searchFragment() {
        // Required empty public constructor
    }


    public static searchFragment newInstance(String param1, String param2) {
        searchFragment fragment = new searchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_search, container, false);

        edt_keyword = v.findViewById(R.id.edt_keyword);
        tv_data = v.findViewById(R.id.tv_data);
        tv_data.setMovementMethod(ScrollingMovementMethod.getInstance());
        btn_searchAll = v.findViewById(R.id.btn_searchAll);
        btn_searchKey = v.findViewById(R.id.btn_searchKey);

        btn_searchAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btn_searchAll.isClickable()) {
                    searchALLGoods();
                }
            }
        });

        btn_searchKey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btn_searchKey.isClickable()) {
                    String keyword = edt_keyword.getText().toString();
                    if(keyword.equals("")){
                        tv_data.setText("Please input the keyword about goods name to search goods");
                    } else {
                        searchKeyword(keyword);
                    }
                }
            }
        });
        return v;
    }

    public void searchKeyword(String keyword){
        List<Goods> goodss = new ArrayList<>();
        String display = "";
        DatabaseHelper db = new DatabaseHelper(getContext());
        goodss = db.searchGoods(keyword);
        for(Goods goods:goodss){
            display += "BID: " + goods.getBid()
                    +"\nGoods Name:" + goods.getGoodsname()
                    +"\nGoods Place: " + goods.getGoodsplace()
                    + "\n\n";
        }
        tv_data.setText(display);
    }

    public void searchALLGoods(){
        List<Goods> goodss = new ArrayList<>();
        String display = "";
        DatabaseHelper db = new DatabaseHelper(getContext());
        goodss = db.getAllGoods();
        for(Goods goods:goodss){
            display += "BID: " + goods.getBid()
                    +"\nGoods Name:" + goods.getGoodsname()
                    +"\nGoods Place:" + goods.getGoodsplace()
                    +"\n\n";
        }
        tv_data.setText(display);
    }
}