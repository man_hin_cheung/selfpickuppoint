package com.example.self_pickuppoint;

import android.content.DialogInterface;
import android.hardware.biometrics.BiometricPrompt;
import android.os.Build;
import android.os.Bundle;
import com.example.self_pickuppoint.DatabaseModule.DatabaseHelper;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.CancellationSignal;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link loginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class loginFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private Button btnLogin;
    private EditText edt_username, edt_password;
    private Button btn_authenticate;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public loginFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment loginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static loginFragment newInstance(String param1, String param2) {
        loginFragment fragment = new loginFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_login, container, false);

        btnLogin = v.findViewById(R.id.btnLogin);
        edt_username = v.findViewById(R.id.edt_username);
        edt_password = v.findViewById(R.id.edt_password);

        btnLogin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String usernameInput = edt_username.getText().toString();
                String passwordInput = edt_password.getText().toString();
                checkLogin(usernameInput, passwordInput);
            }
        });

        //bio
        btn_authenticate = v.findViewById(R.id.btn_authenticate);
        final Executor executor = Executors.newSingleThreadExecutor();
        final loginFragment loginFragment = this;
        final BiometricPrompt biometricPrompt = new BiometricPrompt.Builder(getContext())
                .setTitle(getString(R.string.biometric_title))
                .setSubtitle(getString(R.string.biometric_subtitle))
                .setDescription(getString(R.string.biometric_description))
                .setNegativeButton(getString(R.string.biometric_negative_button_text)
                        , executor, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                            }
                        }).build();

        btn_authenticate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                biometricPrompt.authenticate(new CancellationSignal(), executor, new BiometricPrompt.AuthenticationCallback() {
                    @Override
                    public void onAuthenticationSucceeded(BiometricPrompt.AuthenticationResult result) {
                        loginFragment.getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                DatabaseHelper db = new DatabaseHelper(loginFragment.this.getActivity());
                                String mid = db.checkMember("test", "12345");
                                //display the message in toast as "Login Success".
                                Toast.makeText(loginFragment.this.getActivity(), "Login Success", Toast.LENGTH_LONG);
                                //move on to the book record page
                                Intent i = new Intent(loginFragment.this.getActivity(), GoodsRecordActivity.class);
                                i.putExtra("mid", mid);
                                startActivity(i);
                            }
                        });
                    }
                });
            }
        });
            return v;
    }

    public void checkLogin(String username, String password){
        DatabaseHelper db = new DatabaseHelper(getContext());
        String mid = db.checkMember(username, password);
        //check the login result
        if(mid.equals("no")){
            edt_username.setText("");
            edt_password.setText("");
            Toast.makeText(getContext(), "Login Fail", Toast.LENGTH_LONG).show();
        } else {
            //display the message in toast as "Login Success".
            Toast.makeText(getContext(),"Login Success", Toast.LENGTH_LONG);
            //move on to the goods record page
            Intent i = new Intent(loginFragment.this.getActivity(), GoodsRecordActivity.class);
            i.putExtra("mid", mid);
            startActivity(i);
        }
    }

}